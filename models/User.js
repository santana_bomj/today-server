const { Schema, model, Types } = require('mongoose')

const schema = new Schema({
  vk_id: { type: Number, required: true, unique: true },
  name: { type: String, default: '' },
  avatar: { type: String, default: '' },
  balance: { type: Number, default: 0 },
  cards: [{ type: Types.ObjectId, ref: 'Card' }],
  actions: [{ type: Types.ObjectId, ref: 'Action' }],
  purchases: {
    type: Map,
    of: { type: Types.Mixed },
    default: {}
  },
  logon: { type: Date },
  ts: { type: Date, default: Date.now },
})

module.exports = model('User', schema)