const { Schema, model, Types } = require('mongoose')

const schema = new Schema({
  name: { type: String },
  description: { type: String },
  price: { type: Number },
  type: { type: String },
  active: { type: Boolean, default: false },
  ts: { type: Date, default: Date.now },
})

module.exports = model('Shop', schema)