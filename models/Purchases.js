const { Schema, model, Types } = require('mongoose')

const schema = new Schema({
  shop_id: { type: Types.ObjectId, ref: 'Shop' },
  balance_before: { type: Number, default: 0 },
  type: { type: String },
  ts: { type: Date, default: Date.now },
})

module.exports = model('Purchases', schema)