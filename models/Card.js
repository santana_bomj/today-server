const { Schema, model, Types } = require('mongoose')

const schema = new Schema({
  user_id: { type: Types.ObjectId, ref: 'User' },
  user_name: { type: String },
  user_avatar: { type: String },
  title: { type: String },
  description: { type: String },
  background: { type: String },
  likes: [{ type: Types.ObjectId, ref: 'User' }],
  comments: [{ type: Types.ObjectId, ref: 'Comment' }],
  stories: [{ type: String }],
  stories_like: { type: Number, default: 0 },
  token: { type: String },
  deleted: { type: Boolean, default: false },
  ts: { type: Date, default: Date.now },
})

module.exports = model('Card', schema)