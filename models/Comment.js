const { Schema, model, Types } = require('mongoose')

const schema = new Schema({
  user_id: { type: Types.ObjectId, ref: 'User' },
  card_id: { type: Types.ObjectId, ref: 'Card' },
  message: { type: String },
  deleted: { type: Boolean, default: false },
  ts: { type: Date, default: Date.now },
})

module.exports = model('Comment', schema)