const express = require('express');
const cluster = require('cluster');
const http = require('http');
const formData = require("express-form-data");
const os = require("os");
const fs = require('fs');
const mongoose = require('mongoose')
var checkUser = require('./checkUser')
const User = require('./models/User')
const Card = require('./models/Card')
const Comment = require('./models/Comment')
const vkapi = require("easyvk")
const Config = require('./config')
var cpuCount = require('os').cpus().length;
var workers = [];

const DB_CONNECT = Config.DB_CONNECT
const PORT = process.env.PORT || 80

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function declOfNum(n, titles) {
    return titles[n % 10 === 1 && n % 100 !== 11 ? 0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2];
}

async function start() {
    try {
        await mongoose.connect(DB_CONNECT, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true
        })
        if (cluster.isMaster) {
            for (var i = 0; i < cpuCount ; i += 1) {
                var worker = cluster.fork();
                worker.on('message', function (data) {
                    for (var j in workers) { workers[j].send(data); }
                });
                workers.push(worker);
            }
            cluster.on('exit', (worker, code, signal) => {
                console.log(`worker ${worker.process.pid} died`);
                cluster.fork();
            });

            setInterval(() => {
                getStoriesLike()
                // generateRating() // на хероку не работает запись в файл
            }, 15 * 60 * 1000);
        }


        if (cluster.isWorker) {
            var app = express();
            const options = {
                uploadDir: os.tmpdir(),
                autoClean: true,
                maxFieldsSize: '50mb'
            };
            app.use(formData.parse(options));
            app.use(formData.format());
            app.use(formData.stream());
            app.use(formData.union());

            app.use(function (req, res, next) {
                res.setHeader('Access-Control-Allow-Origin', '*');
                next();
            });

            app.use(function (req, res, next) {
                if (!checkUser(req.body))
                    res.json({ status: 'error', message: 'Ошибка авторизации' })
                else next()
            });

            app.post('/', async (request, response) => {
                try {
                    var newUser = false
                    var user = await User.findOne({ vk_id: request.body.vk_user_id });
                    if (!user) {
                        user = new User({ vk_id: request.body.vk_user_id, name: decodeURIComponent(request.body.name), avatar: request.body.avatar })
                        newUser = true
                        await user.save()
                    }
                    var cards = await Card.aggregate([
                        { $match: { 'deleted': false, 'ts': { $gt: new Date(Date.now() - 24 * 60 * 60 * 1000) } } },
                        { $sort: { ts: -1 } },
                        { $limit: 5 },
                        { $unset: ['deleted', '__v', 'stories'] }
                    ])
                    var data = []
                    var now = new Date(Date.now())
                    cards.forEach(item => {
                        var when
                        var date = new Date(item.ts)
                        var daysLag = Math.ceil(Math.abs(now.getTime() - date.getTime()) / (1000 * 3600 * 24))
                        if (daysLag === 0)
                            when = 'сегодня'
                        else if (daysLag === 1)
                            when = 'вчера'
                        else
                            when = date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear()
                        item = { ...item, when }
                        if (item.likes.filter(item => item.toString() === user._id.toString()).length)
                            item = { ...item, liked: true }
                        item = { ...item, likes: (item.likes ? item.likes.length : 0) + item.stories_like, comments: item.comments ? item.comments.length : 0 }
                        if (item.user_id.toString() === user._id.toString())
                            data.push({ ...item, user_name: 'Я', author: true })
                        else
                            data.push(item)
                    })

                    response.json({ status: 'ok', data, _id: user._id, newUser })
                    if (decodeURIComponent(request.body.user_name) !== '' && decodeURIComponent(request.body.user_name) !== user.name)
                        user.name = decodeURIComponent(request.body.user_name)
                    if (request.body.avatar !== '' && request.body.avatar !== user.avatar)
                        user.avatar = request.body.avatar
                    user.save()
                }
                catch (e) {
                    console.log(e)
                }
            });

            app.post('/getData', async (request, response) => {
                try {
                    var user = await User.findOne({ vk_id: request.body.vk_user_id });
                    if (!user) {
                        response.json({ status: 'error', message: 'Ошибка авторизации' })
                        return false
                    }
                    var cards = await Card.aggregate([
                        { $match: { 'deleted': false, 'ts': { $gt: new Date(Date.now() - 24 * 60 * 60 * 1000), $lt: request.body.last && request.body.last !== 'null' ? new Date(new Date(request.body.last).getTime() - 1) : new Date(Date.now()) } } },
                        { $sort: { ts: -1 } },
                        { $limit: 5 },
                        { $unset: ['deleted', '__v', 'stories'] }
                    ])
                    var data = []
                    var now = new Date(Date.now())
                    cards.forEach(item => {
                        var when
                        var date = new Date(item.ts)
                        var daysLag = Math.ceil(Math.abs(now.getTime() - date.getTime()) / (1000 * 3600 * 24))
                        if (daysLag === 0)
                            when = 'сегодня'
                        else if (daysLag === 1)
                            when = 'вчера'
                        else
                            when = date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear()
                        item = { ...item, when }
                        if (item.likes.filter(item => item.toString() === user._id.toString()).length)
                            item = { ...item, liked: true }
                        item = { ...item, likes: (item.likes ? item.likes.length : 0) + item.stories_like, comments: item.comments ? item.comments.length : 0 }
                        if (item.user_id.toString() === user._id.toString())
                            data.push({ ...item, user_name: 'Я', author: true })
                        else
                            data.push(item)
                    })
                    response.json({ status: 'ok', data })
                }
                catch (e) {
                    console.log(e)
                }
            });

            app.post('/getUser', async (request, response) => {
                try {
                    var user = await User.findOne({ vk_id: request.body.vk_user_id });
                    if (!user) {
                        response.json({ status: 'error', message: 'Ошибка авторизации' })
                        return false
                    }
                    var user_object = {}
                    if (request.body.last === 'null') {
                        var like = await Card.aggregate([
                            { "$match": { "deleted": false, user_id: mongoose.Types.ObjectId(request.body._id) } },
                            {
                                "$group": {
                                    "_id": null,
                                    "like": {
                                        "$sum": { "$size": "$likes" }
                                    },
                                    "count": { $sum: 1 }
                                }
                            }
                        ])
                        if (like) {
                            user_object['likes_sum'] = like[0] && like[0].like ? like[0].like : 0
                            user_object['today_sum'] = like[0] && like[0].count ? like[0].count  : 0
                        } else {
                            user_object['likes_sum'] = 0
                            user_object['today_sum'] = 0
                        }

                        if (user._id.toString() === request.body._id) {
                            user_object['name'] = user.name
                            user_object['avatar'] = user.avatar
                        } else {
                            var get_user = await User.findOne({ _id: request.body._id });
                            if (!get_user)
                                user_object = 'none'
                            else {
                                user_object['name'] = get_user.name
                                user_object['avatar'] = get_user.avatar
                            }
                        }
                    }
                    var cards = await Card.aggregate([
                        { $match: { 'user_id': mongoose.Types.ObjectId(request.body._id), 'deleted': false, 'ts': { $lt: request.body.last !== 'null' ? new Date(new Date(request.body.last).getTime() - 1) : new Date(Date.now()) } } },
                        { $sort: { ts: -1 } },
                        { $limit: 5 },
                        { $unset: ['deleted', '__v', 'stories'] }
                    ])
                    var data = []
                    var now = new Date(Date.now())
                    cards.forEach(item => {
                        var when
                        var date = new Date(item.ts)
                        var daysLag = Math.ceil(Math.abs(now.getTime() - date.getTime()) / (1000 * 3600 * 24))
                        if (daysLag === 0)
                            when = 'сегодня'
                        else if (daysLag === 1)
                            when = 'вчера'
                        else
                            when = date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear()
                        item = { ...item, when }
                        if (item.likes.filter(item => item.toString() === user._id.toString()).length)
                            item = { ...item, liked: true }
                        item = { ...item, likes: (item.likes ? item.likes.length : 0) + item.stories_like, comments: item.comments ? item.comments.length : 0 }
                        if (item.user_id.toString() === user._id.toString())
                            data.push({ ...item, user_name: 'Я', author: true })
                        else
                            data.push(item)
                    })
                    response.json({ status: 'ok', data, user: user_object })
                }
                catch (e) {
                    console.log(e)
                }
            });

            app.post('/add', async (request, response) => {
                try {
                    var user = await User.findOne({ vk_id: request.body.vk_user_id });
                    if (!user) {
                        response.json({ status: 'error', message: 'Ошибка авторизации' })
                        return false
                    }
                    card = new Card({ user_id: user._id, user_name: decodeURIComponent(request.body.user_name), user_avatar: decodeURIComponent(request.body.avatar), title: decodeURIComponent(request.body.title), description: decodeURIComponent(request.body.description), background: decodeURIComponent(request.body.background) })
                    await card.save()
                    user.cards.push(card._id)
                    await user.save()
                    response.json({ status: 'ok', _id: card._id })
                }
                catch (e) {
                    console.log(e)
                }
            });

            app.post('/toggleLike', async (request, response) => {
                try {
                    var user = await User.findOne({ vk_id: request.body.vk_user_id });
                    if (!user) {
                        response.json({ status: 'error', message: 'Ошибка авторизации' })
                        return false
                    }

                    var card = await Card.findOne({ _id: mongoose.Types.ObjectId(request.body._id) })
                    if (card.likes.filter(item => item.toString() === user._id.toString()).length) {
                        card.likes = card.likes.filter(item => item.toString() !== user._id.toString())
                    } else {
                        card.likes.push(user._id)
                    }
                    await card.save()
                    response.json({ status: 'ok' })
                }
                catch (e) {
                    console.log(e)
                }
            });

            app.post('/getLike', async (request, response) => {
                try {
                    var user = await User.findOne({ vk_id: request.body.vk_user_id });
                    if (!user) {
                        response.json({ status: 'error', message: 'Ошибка авторизации' })
                        return false
                    }
                    var card = await Card.findOne({ _id: request.body._id })
                    if (!card) {
                        response.json({ status: 'error', message: '_id не найден' })
                        return false
                    }
                    var users = await User.aggregate([
                        { $match: { '_id': { '$in': card.likes } } },
                    ])

                    var data = users.map(item => {
                        return { _id: item._id, name: item.name, avatar: item.avatar }
                    })

                    if (card.stories_like > 0)
                        data.push({ _id: '1', name: card.stories_like + ' ' + declOfNum(card.stories_like, ['лайк', 'лайка', 'лайков']) + ' с историй', avatar: 'https://sun9-45.userapi.com/c853516/v853516534/2202c6/a9bRuxy1XK4.jpg' }) // склонение лайков + аватар

                    response.json({ status: 'ok', data })
                }
                catch (e) {
                    console.log(e)
                }
            });

            app.post('/addComment', async (request, response) => {
                try {
                    var user = await User.findOne({ vk_id: request.body.vk_user_id });
                    if (!user) {
                        response.json({ status: 'error', message: 'Ошибка авторизации' })
                        return false
                    }
                    var card = await Card.findOne({ _id: request.body._id })
                    if (!card) {
                        response.json({ status: 'error', message: '_id не найден' })
                        return false
                    }

                    var comment = new Comment({ user_id: user._id, card_id: card._id, message: decodeURIComponent(request.body.message) })
                    await comment.save()
                    card.comments.push(comment._id)
                    await card.save()

                    response.json({ status: 'ok', _id: comment._id, ts: comment.ts })
                }
                catch (e) {
                    console.log(e)
                }
            });

            app.post('/getComments', async (request, response) => {
                try {
                    var user = await User.findOne({ vk_id: request.body.vk_user_id });
                    if (!user) {
                        response.json({ status: 'error', message: 'Ошибка авторизации' })
                        return false
                    }
                    var card = await Card.findOne({ _id: request.body._id })
                    if (!card) {
                        response.json({ status: 'error', message: '_id не найден' })
                        return false
                    }
                    var comments = await Comment.aggregate([
                        { $match: { '_id': { '$in': card.comments }, deleted: false } },
                        {
                            $lookup:
                            {
                                from: "users",
                                localField: "user_id",
                                foreignField: "_id",
                                as: "user"
                            }
                        }
                    ])

                    var data = comments.map(item => {
                        return { _id: item._id, message: item.message, ts: item.ts, name: item.user[0].name, avatar: item.user[0].avatar }
                    })

                    response.json({ status: 'ok', data })
                }
                catch (e) {
                    console.log(e)
                }
            });

            app.post('/addStories', async (request, response) => {
                try {
                    var user = await User.findOne({ vk_id: request.body.vk_user_id });
                    if (!user) {
                        response.json({ status: 'error', message: 'Ошибка авторизации' })
                        return false
                    }

                    var card = await Card.findOne({ _id: mongoose.Types.ObjectId(request.body._id) })
                    card.stories.push(request.body.stories)
                    await card.save()
                    response.json({ status: 'ok' })
                }
                catch (e) {
                    console.log(e)
                }
            });
            app.post('/addStoriesToken', async (request, response) => {
                try {
                    var user = await User.findOne({ vk_id: request.body.vk_user_id });
                    if (!user) {
                        response.json({ status: 'error', message: 'Ошибка авторизации' })
                        return false
                    }
                    var stories = JSON.parse(request.body.stories)
                    for (var [key, value] of Object.entries(stories)) {
                        var card = await Card.findOne({ _id: mongoose.Types.ObjectId(key) })
                        card.stories.push(value)
                        card.token = request.body.token
                        await card.save()
                    }
                    response.json({ status: 'ok' })
                }
                catch (e) {
                    console.log(e)
                }
            });
            app.post('/delete', async (request, response) => {
                try {
                    var user = await User.findOne({ vk_id: request.body.vk_user_id });
                    if (!user) {
                        response.json({ status: 'error', message: 'Ошибка авторизации' })
                        return false
                    }
                    await Card.updateOne({ _id: request.body._id, user_id: user._id }, { $set: { deleted: true } })
                    response.json({ status: 'ok' })
                }
                catch (e) {
                    console.log(e)
                }
            });

            app.post('/getRating', async (request, response) => {
                try {
                    var user = await User.findOne({ vk_id: request.body.vk_user_id });
                    if (!user) {
                        response.json({ status: 'error', message: 'Ошибка авторизации' })
                        return false
                    }
                    // не работает на хероку
                    /* let rating = JSON.parse(fs.readFileSync('rating.json', 'utf8'));
                    response.json({ status: 'ok', data: rating }) */
                    var card_rating = []
                    var cards = await Card.aggregate([
                        {
                            $match: {
                                user_id: { $ne: null },
                                deleted: false
                            }
                        },
                        {
                            $group: {
                                _id: "$user_id",
                                count: {
                                    $sum: {
                                        $add: [
                                            { "$size": "$likes" }, '$stories_like'
                                        ]
                                    }
                                }
                            }
                        },
                        { $sort: { count: -1 } },
                        { $limit: 13 },
                        {
                            $lookup:
                            {
                                from: "users",
                                localField: "_id",
                                foreignField: "_id",
                                as: "user"
                            }
                        }
                    ])
                    cards.forEach(card => {
                        card_rating.push({ _id: card._id, rating: card.count, name: card.user[0].name, avatar: card.user[0].avatar })
                    });
                    response.json({ status: 'ok', data: card_rating })
                }
                catch (e) {
                    console.log(e)
                }
            });

            app.post('/getFriendsRating', async (request, response) => {
                try {
                    var user = await User.findOne({ vk_id: request.body.vk_user_id });
                    if (!user) {
                        response.json({ status: 'error', message: 'Ошибка авторизации' })
                        return false
                    }
                    var users = await User.aggregate([
                        { $match: { 'vk_id': { '$in': JSON.parse(request.body.friends) } } },
                    ])

                    var card_rating = []
                    var cards = await Card.aggregate([
                        {
                            $match: {
                                user_id: { $in: users.map(item => item._id) },
                                deleted: false
                            }
                        },
                        {
                            $group: {
                                _id: "$user_id",
                                count: {
                                    $sum: {
                                        $add: [
                                            { "$size": "$likes" }, '$stories_like'
                                        ]
                                    }
                                }
                            }
                        },
                        { $sort: { count: -1 } },
                        { $limit: 13 },
                        {
                            $lookup:
                            {
                                from: "users",
                                localField: "_id",
                                foreignField: "_id",
                                as: "user"
                            }
                        }
                    ])
                    cards.forEach(card => {
                        card_rating.push({ _id: card._id, rating: card.count, name: card.user[0].name, avatar: card.user[0].avatar })
                    });
                    response.json({ status: 'ok', data: card_rating })
                }
                catch (e) {
                    console.log(e)
                }
            });

            http.createServer(app).listen(PORT, function () {
                console.log("Express server listening on port " + PORT + " as Worker " + cluster.worker.id + " running @ process " + cluster.worker.process.pid + "!");
            });
        }
    } catch (e) {
        console.log('Server Error', e.message)
        process.exit(1)
    }
}

start()

async function getStoriesLike() {
    try {
        var cards = await Card.find({ deleted: false, 'ts': { $gt: new Date(Date.now() - 24 * 60 * 60 * 1000) }, "stories.0": { $exists: true }, "token": { $ne: '' } })
        cards.forEach(async (element) => {
            await vkapi({
                access_token: element.token
            }).then(async (vk) => {
                var res = await vk.call('stories.getById', { stories: element.stories.join(',') })
                var like = 0
                var active_stories = []
                res.items.forEach(item => {
                    if (item.likes_count >= 0) {
                        active_stories.push(item.owner_id + '_' + item.id)
                        like = like + item.likes_count
                    }
                });
                await Card.updateOne({ _id: element._id }, { $set: { stories: active_stories, stories_like: like } })
            }).catch(async (error) => {
                await Card.updateOne({ _id: element._id }, { $set: { toke: '' } })
            });
            await sleep(1)
        });
    }
    catch (e) {
        console.log('Server Error', e.message)
    }
}

async function generateRating() {
    try {
        console.log('start rating')
        var card_rating = []
        var cards = await Card.aggregate([
            {
                $match: {
                    user_id: { $ne: null },
                    deleted: false
                }
            },
            {
                $group: {
                    _id: "$user_id",
                    count: {
                        $sum: {
                            $add: [
                                { "$size": "$likes" }, '$stories_like'
                            ]
                        }
                    }
                }
            },
            { $sort: { count: -1 } },
            { $limit: 13 },
            {
                $lookup:
                {
                    from: "users",
                    localField: "_id",
                    foreignField: "_id",
                    as: "user"
                }
            }
        ])
        cards.forEach(card => {
            card_rating.push({ _id: card._id, rating: card.count, name: card.user[0].name, avatar: card.user[0].avatar })
        });
        fs.writeFile('rating.json', JSON.stringify(card_rating), 'utf8', () => { });
        console.log('end rating')
    }
    catch (e) {
        console.log('Server Error', e.message)
    }
}