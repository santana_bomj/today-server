const qs = require('querystring');
const crypto = require('crypto');

module.exports = (data) => {
    try {

        const urlParams = data;
        const ordered = {};
        Object.keys(urlParams).sort().forEach((key) => {
            if (key.slice(0, 3) === 'vk_') {
                ordered[key] = decodeURIComponent(urlParams[key]);
            }
        });
        const stringParams = qs.stringify(ordered);
        const paramsHash = crypto
            .createHmac('sha256', 'DZKxXdyqZ5kDruAxDvIi')
            .update(stringParams)
            .digest()
            .toString('base64')
            .replace(/\+/g, '-')
            .replace(/\//g, '_')
            .replace(/=$/, '');

        if (paramsHash === urlParams.sign)
            return true;
        else
            return false

    } catch (e) {
        return false
    }
}